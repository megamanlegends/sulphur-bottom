/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  distDir: 'dist',
  images: {
    domains: ['github.com'],
  },
}
