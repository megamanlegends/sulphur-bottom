import React, { ChangeEvent, useRef, useState } from 'react';
import db from '../store/database';
import Modal from '@mui/material/Modal';
import LinearProgress from '@mui/material/LinearProgress';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

type ProgressModelProps = {
	open: boolean;
	filename: string;
	progress: number;
};

const style = {
	position: 'absolute' as 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4,
};

const ButtonAppBar = () => {};

const ProgressModal = ({ open, filename, progress }: ProgressModelProps) => {
	return (
		<Modal
			open={open}
			aria-labelledby='modal-modal-title'
			aria-describedby='modal-modal-description'
		>
			<Box sx={style}>
				<Typography id='modal-modal-title' variant='h6' component='h2'>
					Scanning Progress
				</Typography>
				<Box id='modal-modal-description' sx={{ mt: 2 }}>
					<Box sx={{ width: '100%' }}>
						<Typography id='modal-modal-title'>
							{filename}
						</Typography>
						<LinearProgress
							variant='determinate'
							value={progress}
						/>
					</Box>
				</Box>
			</Box>
		</Modal>
	);
};

type NavbarParams = {
	scene: string | undefined;
	setScene: (scene: string) => void;
	setSceneList: (scenes: string[]) => void;
};

const MyNavbar = ({ scene, setScene, setSceneList }: NavbarParams) => {
	const [open, setOpen] = useState(false);
	const [progress, setProgress] = useState(0);
	const [filename, setFilename] = useState('');
	const inputRef = useRef<HTMLInputElement>(null);

	const handleImportClick = () => {
		(inputRef.current! as HTMLInputElement).click();
	};

	const handleImportChange = async (evt: ChangeEvent<HTMLInputElement>) => {
		if (!process.browser) {
			return;
		}

		// Get the files from the on change event
		const { files } = evt.target;
		if (!files) {
			return;
		}

		setOpen(true);
		// Store the files into local storage to be references
		for (let i = 0; i < files.length; i++) {
			setFilename(files[i].name);
			setProgress(Math.floor((i / files.length) * 100));
			await db.scanFile(files[i]);
		}

		// await db.scan(setProgress, setFilename);
		const newSceneList = await db.getScenes();
		setSceneList(newSceneList);
		setOpen(false);

		if (!scene) {
			setScene(newSceneList[0]);
		}

	};

	return (
		<AppBar position='static'>
			<Toolbar>
				<IconButton
					size='large'
					edge='start'
					color='inherit'
					aria-label='menu'
					sx={{ mr: 2 }}
				>
					<MenuIcon />
				</IconButton>
				<Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
					Suplhur Bottom
				</Typography>
				<Button color='inherit' onClick={()=>{inputRef.current?.click()}}>Import</Button>
				<input
					ref={inputRef}
					type='file'
					style={{ display: 'none' }}
					accept='.bin, .BIN'
					multiple={true}
					onChange={handleImportChange}
				/>
			</Toolbar>
			<ProgressModal
				open={open}
				filename={filename}
				progress={progress}
			/>
		</AppBar>
	);
};

export default MyNavbar;
