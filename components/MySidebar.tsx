import React, { useState, useEffect, useRef, CSSProperties } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { ColorData, IEntity, ImageCoordinate, ITexture } from '../store/types';
import db from '../store/database';
import { Radio, TextField } from '@mui/material';
import cx from 'classnames';

interface TabPanelProps {
	index: number;
	value: number;
	data: ImageCoordinate | undefined;
}

type SidebarParams = {
	asset: IEntity | undefined;
};

type ColorPaletteParams = {
	palette: ColorData[];
	index: number;
	selected: boolean;
};

const defaultPalette = new Array(16);
for (let i = 0; i < defaultPalette.length; i++) {
	const r = 16 * i;
	const g = 16 * i;
	const b = 16 * i;
	const a = 1;
	defaultPalette[i] = { r, g, b, a };
}

const ColorPalette = ({ palette, index, selected }: ColorPaletteParams) => {
	return (
		<div className='d-flex flex-row bd-highlight mt-1'>
			<Radio checked={selected} value={index} name='radio-buttons' />
			{palette.map((texel, index) => {
				const { r, g, b, a } = texel;
				const color = `rgba(${r},${g},${b},${a})`;
				const key = `${name}-${index}`;
				const texelStyle: CSSProperties = {
					backgroundColor: color,
					minHeight: 16,
					width: 'calc(100% / 16)',
					borderRight: '1px solid #9a9a9a',
					borderBottom: '1px solid #9a9a9a',
					borderTop: '1px solid #9a9a9a',
				};
				if (index === 0) {
					texelStyle.borderLeft = '1px solid #9a9a9a';
				}
				return (
					<div key={key} style={texelStyle} className='py-1'></div>
				);
			})}
		</div>
	);
};

const TabPanel = ({ value, index, data }: TabPanelProps) => {
	const [image, setImage] = useState<ITexture | null>(null);
	const [imageName, setImageName] = useState('');
	const [palette, setPallete] = useState<ITexture | null>(null);
	const [paletteName, setPaletteName] = useState('');
	const [paletteIndex, setPaletteIndex] = useState(0);
	const canvasRef = useRef<HTMLCanvasElement>(null);

	useEffect(() => {
		const canvas = canvasRef.current;
		if (!canvas || !image || !palette) {
			return;
		}

		const context = canvas!.getContext('2d');

		const { imageX, imageY, height, width, imageData } = image;
		const xOfs = ((imageX - 320) % 64) * 4;
		const yOfs = imageY % 256;

		let ofs = 0;
		for (let y = 0; y < height; y++) {
			for (var x = 0; x < width; x++) {
				const colorIndex = imageData![ofs++];
				const { r, g, b, a } =
					palette.palettes[paletteIndex][colorIndex];
				context!.fillStyle = `rgba(${r},${g},${b},${a})`;
				context!.fillRect(x + xOfs, y + yOfs, 1, 1);
			}
		}
	}, [image, palette, paletteIndex]);

	const handleImageChange = async (event: React.ChangeEvent) => {
		const { value } = event.target as HTMLInputElement;
		setImageName(value);
		const selectedImage = await db.selectImageByName(value);
		if (!selectedImage || !selectedImage.imageData) {
			return;
		}
		setImage(selectedImage);

		if (
			(!palette || palette.name === 'default') &&
			selectedImage.palettes.length
		) {
			setPaletteIndex(0);
			setPaletteName(value);
			setPallete(selectedImage);
			return;
		}

		if (palette) {
			return;
		}

		setPaletteIndex(0);
		setPallete({
			name: 'default',
			sceneName: 'none',
			width: -1,
			height: -1,
			imageX: -1,
			imageY: -1,
			imageData: undefined,
			imagePage: -1,
			paletteX: -1,
			paletteY: -1,
			palettes: [defaultPalette],
			topLeft: false,
			topRight: false,
			bottomLeft: false,
			bottomRight: false,
		});
	};

	const handlePaletteChange = async (event: React.ChangeEvent) => {
		const { value } = event.target as HTMLInputElement;
		setPaletteName(value);
		const selectedImage = await db.selectImageByName(value);
		if (!selectedImage || !selectedImage.palettes.length) {
			return;
		}
		setPaletteIndex(0);
		setPallete(selectedImage);
	};

	if (!data) {
		return <></>;
	}

	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			style={{
				height: 'calc(100% - 70px)',
				position: 'relative',
				width: '100%',
				padding: '10px',
				overflowY: 'auto',
			}}
		>
			<Typography variant='h5' align='center'>
				Image
			</Typography>

			<TextField
				label='Image Name'
				variant='filled'
				fullWidth
				onChange={handleImageChange}
				value={imageName}
			/>

			<div className='my-2'>
				<div className='d-flex w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Image Page: </strong>
					{db.calcImagePage(data.image.x, data.image.y)}
				</div>
				<div className='d-flex w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Image Coords: </strong>
					0x{data.image.coords.toString(16)}
				</div>
				<div className='d-flex w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Image X</strong> {data.image.x}
					<strong className='mb-1'>Image Y</strong> {data.image.y}
				</div>
			</div>

			<canvas
				ref={canvasRef}
				width={256}
				height={256}
				style={{
					border: '1px solid #8a8a8a',
					display: 'block',
					margin: 'auto',
					marginBottom: '10px',
				}}
			/>

			<Typography variant='h5' align='center'>
				Palettes
			</Typography>

			<TextField
				label='Palette Name'
				variant='filled'
				fullWidth
				value={paletteName}
				onChange={handlePaletteChange}
			/>

			<div className='my-2'>
				<div className='d-flex w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Palette Index: </strong>{' '}
					{paletteIndex}
				</div>

				<div className='d-flex w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Palette X</strong> {data.palette.x}
					<strong className='mb-1'>Palette Y</strong> {data.palette.y}
				</div>
			</div>

			{palette && (
				<>
					{palette.palettes.map((pal, index) => {
						return (
							<div
								key={`${palette.name}-${index}`}
								onClick={() => {
									setPaletteIndex(index);
								}}
							>
								<ColorPalette
									palette={pal}
									index={index}
									selected={index === paletteIndex}
								/>
							</div>
						);
					})}
				</>
			)}
		</div>
	);
};

export const MySidebar = ({ asset }: SidebarParams) => {
	const [tab, setTab] = useState(0);

	useEffect(() => {
		setTab(0);
	}, [asset]);

	const handleChange = (event: React.SyntheticEvent, newTab: number) => {
		setTab(newTab);
	};

	const tabCount = asset ? asset.data.textureCoords.length : 0;

	return (
		<Box sx={{ width: '100%', height: '100%' }}>
			<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
				<Tabs
					value={tab}
					onChange={handleChange}
					aria-label='basic tabs example'
					variant='fullWidth'
				>
					<Tab
						label='One'
						id='simple-tab-0'
						aria-controls='simple-tabpanel-0'
						disabled={tabCount < 1}
					/>
					<Tab
						label='Two'
						id='simple-tab-0'
						aria-controls='simple-tabpanel-0'
						disabled={tabCount < 2}
					/>
					<Tab
						label='Three'
						id='simple-tab-0'
						aria-controls='simple-tabpanel-0'
						disabled={tabCount < 3}
					/>
					<Tab
						label='Four'
						id='simple-tab-0'
						aria-controls='simple-tabpanel-0'
						disabled={tabCount < 4}
					/>
				</Tabs>
			</Box>
			<TabPanel
				value={tab}
				index={0}
				data={asset ? asset.data.textureCoords[0] : undefined}
			/>
			<TabPanel
				value={tab}
				index={1}
				data={asset ? asset.data.textureCoords[1] : undefined}
			/>
			<TabPanel
				value={tab}
				index={2}
				data={asset ? asset.data.textureCoords[2] : undefined}
			/>
			<TabPanel
				value={tab}
				index={3}
				data={asset ? asset.data.textureCoords[3] : undefined}
			/>
		</Box>
	);
};

export default MySidebar;
