import React, { useEffect, useRef, useState } from 'react';
import cx from 'classnames';
import db from '../store/database';
import styles from '../styles/MyTray.module.css';
import { ColorData, IEntity, ITexture } from '../store/types';
import { names } from '../store/names';
import { entityMesh } from '../store/Entity/mesh';
import { Mesh } from 'three';

enum AssetType {
	Entity = 'Entity',
	StageTile = 'Stage Tile',
	Texture = 'Texture',
}

type TrayParams = {
	scene: string | undefined;
	setMesh: (mesh: Mesh) => void;
	setAsset: (asset: IEntity) => void;
};

type EntityItemParams = {
	asset: IEntity;
	isActive: boolean;
	setActiveItem: (key: string) => void;
	parentKey: string;
	setMesh: (mesh: Mesh) => void;
	setAsset: (asset: IEntity) => void;
};

type TextureItemParams = {
	asset: ITexture;
};

type ColorPaletteParams = {
	palette: ColorData[];
	name: string;
};

const defaultPalette = new Array(16);
for (let i = 0; i < defaultPalette.length; i++) {
	const r = 16 * i;
	const g = 16 * i;
	const b = 16 * i;
	const a = 1;
	defaultPalette[i] = { r, g, b, a };
}

const ColorPalette = ({ palette, name }: ColorPaletteParams) => {
	return (
		<div className='d-flex flex-row bd-highlight mt-1'>
			{palette.map((texel, index) => {
				const { r, g, b, a } = texel;
				const color = `rgba(${r},${g},${b},${a})`;
				const key = `${name}-${index}`;
				return (
					<div
						key={key}
						style={{
							backgroundColor: color,
							minHeight: 20,
							width: 'calc(100% / 16)',
						}}
						className='py-1'
					></div>
				);
			})}
		</div>
	);
};

const TextureItem = ({ asset }: TextureItemParams) => {
	const canvasRef = useRef<HTMLCanvasElement>(null);

	const [filename, offset] = asset.name.split('_');
	useEffect(() => {
		const canvas = canvasRef.current;
		if (!canvas) {
			return;
		}
		const context = canvas!.getContext('2d');
		//Our first draw

		if (!asset.imageData) {
			return;
		}

		const palette = asset.palettes[0] || defaultPalette;
		if (!palette) {
			return;
		}

		const xOfs = ((asset.imageX - 320) % 64) * 4;
		const yOfs = asset.imageY % 256;

		let ofs = 0;
		for (let y = 0; y < asset.height; y++) {
			for (var x = 0; x < asset.width; x++) {
				const colorIndex = asset.imageData![ofs++];
				const { r, g, b, a } = palette[colorIndex];
				context!.fillStyle = `rgba(${r},${g},${b},${a})`;
				context!.fillRect(x + xOfs, y + yOfs, 1, 1);
			}
		}
	}, [asset]);

	const handleClick = async () => {
		await navigator.clipboard.writeText(asset.name);
	};

	return (
		<a
			className={cx([
				'list-group-item',
				'list-group-item-action',
				'py-3',
				'lh-tight',
			])}
			aria-current='true'
			onClick={handleClick}
		>
			<div className='d-flex w-100 align-items-center justify-content-between'>
				<strong className='mb-1'>Filename</strong> {filename}
			</div>

			<div className='d-flex w-100 align-items-center justify-content-between'>
				<strong className='mb-1'>Scene</strong> {asset.sceneName}
				<strong className='mb-1'>Offset</strong> {offset}
			</div>

			{asset.imageData && (
				<>
					<div className='d-flex w-100 align-items-center justify-content-between'>
						<strong className='mb-1'>Image Page: </strong>{' '}
						{asset.imagePage}
					</div>
					<div className='d-flex w-100 align-items-center justify-content-between'>
						<strong className='mb-1'>Width</strong> {asset.width}
						<strong className='mb-1'>Height</strong> {asset.height}
					</div>
					<div className='d-flex w-100 align-items-center justify-content-between'>
						<strong className='mb-1'>TL</strong>{' '}
						{asset.topLeft.toString()}
						<strong className='mb-1'>TR</strong>{' '}
						{asset.topRight.toString()}
					</div>
					<div className='d-flex w-100 align-items-center justify-content-between'>
						<strong className='mb-1'>BL</strong>{' '}
						{asset.bottomLeft.toString()}
						<strong className='mb-1'>BR</strong>{' '}
						{asset.bottomRight.toString()}
					</div>
					<div className='d-flex w-100 align-items-center justify-content-between'>
						<strong className='mb-1'>Image X</strong>{' '}
						{asset.imageX.toString()}
						<strong className='mb-1'>Image Y</strong>{' '}
						{asset.imageY.toString()}
					</div>
					<canvas
						ref={canvasRef}
						width={256}
						height={256}
						style={{
							border: '1px solid #8a8a8a',
							display: 'block',
							margin: 'auto',
						}}
					/>
				</>
			)}
			{asset.palettes.length && (
				<div className='d-flex pt-1 w-100 align-items-center justify-content-between'>
					<strong className='mb-1'>Palette X</strong> {asset.paletteX}
					<strong className='mb-1'>Palette Y</strong> {asset.paletteY}
				</div>
			)}
			{asset.palettes.map((palette, index) => {
				const key = `${asset.name}-${index}`;
				return <ColorPalette key={key} palette={palette} name={key} />;
			})}
		</a>
	);
};

const EntityItem = ({
	asset,
	isActive,
	setActiveItem,
	parentKey,
	setMesh,
	setAsset,
}: EntityItemParams) => {
	const { data } = asset;

	const handleTrayClick = async () => {
		setActiveItem(parentKey);
		setAsset(asset);
		const mesh = await entityMesh(asset.data, asset.sceneName);
		setMesh(mesh);
	};

	return (
		<a
			onClick={handleTrayClick}
			className={cx(
				[
					'list-group-item',
					'list-group-item-action',
					'py-3',
					'lh-tight',
				],
				{ active: isActive },
			)}
			aria-current='true'
		>
			<div className='d-flex w-100 align-items-center justify-content-between'>
				<strong className='mb-1'>
					{names[asset.id.toString(16)]
						? names[asset.id.toString(16)].name
						: 'Character Name'}
				</strong>
				<small>{asset.id.toString(16)}</small>
			</div>
			<div className='col-10 mb-1 small'>
				Bone Count: {data.lod.high.length}
			</div>
			<div className='col-10 mb-1 small'>
				Texture Counts: {data.textureCoords.length}
			</div>
		</a>
	);
};

const MyTray = ({ scene, setMesh, setAsset }: TrayParams) => {
	const tabs = [AssetType.Entity, AssetType.StageTile, AssetType.Texture];
	const [assetList, setAssetList] = useState<IEntity[] | ITexture[]>([]);
	const [tab, setTab] = useState(tabs[0]);
	const [activeItem, setActiveItem] = useState('');

	useEffect(() => {
		db.select(tab, scene || '').then((assets) => {
			setAssetList(assets);
		});
	}, [scene, tab]);

	return (
		<div
			className={cx(
				'd-flex flex-column align-items-stretch flex-shrink-0 bg-white',
				styles.tray,
			)}
		>
			<div className='d-flex align-items-center flex-shrink-0 p-1 link-dark text-decoration-none border-bottom'>
				<ul className='nav nav-pills nav-fill w-100'>
					{tabs.map((tabName) => {
						const tabClass = cx({
							'nav-link': true,
							disabled: tabName === AssetType.StageTile,
							active: tabName === tab,
						});
						return (
							<a
								className={tabClass}
								key={tabName}
								aria-current='page'
								onClick={() => {
									setTab(tabName);
								}}
							>
								{tabName}
							</a>
						);
					})}
				</ul>
			</div>
			<div
				className={cx(
					'list-group list-group-flush border-bottom',
					styles.scrollarea,
				)}
			>
				{assetList.map((asset) => {
					let key = '';

					switch (tab) {
						case AssetType.Entity:
							const entity = asset as IEntity;
							if (!entity.id) {
								return;
							}
							key = [entity.id, entity.sceneName, tab].join('-');
							return (
								<EntityItem
									key={key}
									asset={entity}
									isActive={key === activeItem}
									setActiveItem={setActiveItem}
									parentKey={key}
									setMesh={setMesh}
									setAsset={setAsset}
								/>
							);
						case AssetType.StageTile:
							return null;
						case AssetType.Texture:
							const texture = asset as ITexture;
							if (!texture.name) {
								return;
							}
							key = [texture.name, tab].join('-');
							return <TextureItem key={key} asset={texture} />;
					}
				})}
			</div>
		</div>
	);
};

export default MyTray;
