import React from 'react';
import cx from 'classnames';
import styles from '../styles/MyScene.module.css';

type FolderParams = {
	scene: string | undefined;
	sceneList: string[];
	setScene: (scene: string) => void;
};

const MyScene = ({ scene, sceneList, setScene }: FolderParams) => {
	const handleFolderClick = (evt: React.MouseEvent<HTMLLIElement>) => {
		const { currentTarget } = evt;
		const sceneName = currentTarget.getAttribute('data-name');
		setScene(sceneName!);
	};

	return (
		<div
			className={cx(
				'd-flex flex-column flex-shrink-0 p-3 text-white bg-dark',
				styles.folder,
			)}
		>

			<h2 className='text-light fs-6 pb-2 border-bottom text-center'>Stage Select</h2>
			<ul className='nav nav-pills flex-column mb-auto'>
				{sceneList.map((s) => {
					const isActive = s === scene;
					const liClass = cx(
						{
							'nav-item': isActive,
						},
						styles.menuItem,
					);
					const aClass = cx({
						'nav-link': true,
						active: isActive,
						'text-white': !isActive,
					});

					return (
						<li
							key={s}
							className={liClass}
							data-name={s}
							onClick={handleFolderClick}
						>
							<a className={aClass} aria-current='page'>
								{s}
							</a>
						</li>
					);
				})}
			</ul>
		</div>
	);
};

export default MyScene;
