import React, { Component, CSSProperties } from 'react';
import {
	Scene,
	PerspectiveCamera,
	WebGLRenderer,
	Mesh,
	PointLight,
	GridHelper,
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

const backdropStyle: CSSProperties = {
	backgroundColor: '#8b95a6',
	width: '100%',
	top: 0,
	left: 0,
	height: '100%',
	position: 'absolute',
};

type CanvasParams = {
	mesh: Mesh | null;
};

type MyCanvasParams = {
	mesh: Mesh | null;
};

// Example: https://codesandbox.io/s/github/supromikali/react-three-demo?file=/src/index.js:3776-3862

class Canvas extends Component<CanvasParams> {
	mount: HTMLCanvasElement | null;
	renderer: WebGLRenderer | null;
	camera: PerspectiveCamera;
	scene: Scene;
	mesh: Mesh | null;
	grid: GridHelper;

	constructor(props: CanvasParams) {
		super(props);
		this.mount = null;
		this.renderer = null;
		this.camera = new PerspectiveCamera(75, 1, 0.1, 1000);
		this.scene = new Scene();
		this.mesh = null;
		this.grid = new GridHelper(50, 20, 0xff0000, 0xffffff);
	}

	componentDidMount() {
		this.createScene();
        if(this.mesh) {
		    this.scene.add(this.mesh!);
        }
		this.updateDimensions();
		this.animationLoop();
		window.addEventListener('resize', this.updateDimensions);
	}

	createScene() {
		this.camera.position.z = 5;
		this.renderer = new WebGLRenderer({
			canvas:this.mount!,
			antialias: true,
			alpha : true,
			preserveDrawingBuffer: true
		});
		this.renderer.setClearColor(0x000000, 0);
		const controls = new OrbitControls(
			this.camera,
			this.mount as HTMLElement,
		);
		const lights: PointLight[] = [];
		lights[0] = new PointLight(0xffffff, 1, 0);
		lights[1] = new PointLight(0xffffff, 1, 0);
		lights[2] = new PointLight(0xffffff, 1, 0);

		lights[0].position.set(0, 200, 0);
		lights[1].position.set(100, 200, 100);
		lights[2].position.set(-100, -200, -100);

		this.scene.add(lights[0]);
		this.scene.add(lights[1]);
		this.scene.add(lights[2]);
		this.scene.add(this.grid);

	}

	updateDimensions() {
        if(!this.mount) {
            return;
        }
		const { clientWidth, clientHeight } = this.mount;
		this.renderer!.setSize(clientWidth, clientHeight);
		this.camera.aspect = clientWidth / clientHeight;
		this.camera.updateProjectionMatrix();
	}

	animationLoop() {
		requestAnimationFrame(this.animationLoop.bind(this));

		if (this.mesh) {
			//this.mesh!.rotation.x += 0.01;
			//this.mesh!.rotation.y += 0.01;
		}
		this.renderer!.render(this.scene, this.camera);
	}

	render() {
        const { mesh } = this.props;
		if(this.mesh) {
			this.scene.remove(this.mesh)
		}

        if(mesh) {
			this.scene.add(mesh)
		}

		this.mesh = mesh;
		return <canvas style={backdropStyle} ref={(ref) => (this.mount = ref)} />;
	}
}

const MyCanvas = ({ mesh }: MyCanvasParams) => {
	return <Canvas mesh={mesh} />;
};

export default MyCanvas;
