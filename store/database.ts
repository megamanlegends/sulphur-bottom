import Dexie from 'dexie';
import { checkForEntities } from './Entity/binary';
import { checkForTextures, checkForPalettes } from './Texture/binary';
import {
	ColorData,
	IEntity,
	IFile,
	IScene,
	ITexture,
	ModelData,
	TimHeader,
	ImageData,
	FramebufferCoord,
	ImageCoordinate,
} from './types';

class FileStorage extends Dexie {
	files!: Dexie.Table<IFile, number>;
	scenes!: Dexie.Table<IScene, number>;
	entities!: Dexie.Table<IEntity, number>;
	textures!: Dexie.Table<ITexture, number>;

	constructor() {
		super('SulphurBottom');
		this.version(1).stores({
			files: '&name, data',
			scenes: '&name',
			entities: '&[sceneName+id], files, data',
			textures:
				'&name,sceneName,[imageX+imageY],imagePage,[paletteX+paletteY],bottomLeft,bottomRight,topLeft,topRight',
		});
	}

	async getScenes(): Promise<string[]> {
		// Get list of scenes in the database
		const scenes = await this.scenes.toArray();
		return scenes.map((scene) => scene.name).sort();
	}

	insertFile(file: File): Promise<void> {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = (evt) => {
				this.files.put({
					name: file.name,
					data: <ArrayBuffer>evt.target!.result,
				});
				resolve();
			};
			reader.onerror = (err) => {
				reject(err);
			};
			reader.readAsArrayBuffer(file);
		});
	}

	readFile(file: File): Promise<IFile> {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = (evt) => {
				resolve({
					name: file.name,
					data: <ArrayBuffer>evt.target!.result,
				});
			};
			reader.onerror = (err) => {
				reject(err);
			};
			reader.readAsArrayBuffer(file);
		});
	}

	async insertEntity(
		filename: string,
		id: number,
		data: ModelData,
	): Promise<void> {
		// Store scene name and file with entity for tracing
		const sceneName = filename.startsWith('ST')
			? filename.substring(0, 4)
			: 'COMMON';
		const files = [filename];

		// Check to see if the entity already exists in the database
		const exists = await this.entities.get({ sceneName, id });

		// If it exists we make sure we save which file it came from
		if (exists) {
			exists.files.forEach((file) => {
				if (files.indexOf(file) !== -1) {
					return;
				}
				files.push(file);
			});
			files.sort();
		}

		// Upsert the scene name into the database
		this.scenes.put({ name: sceneName });

		// Upsert the file into the database
		this.entities.put({
			sceneName,
			id,
			files,
			data,
		});
	}

	calcImagePage(x: number, y: number): number {
		let page = Math.floor((x - 320) / 64);
		if (y >= 256) {
			page += 11;
		}
		return page;
	}

	insertTexture(
		filename: string,
		ofs: number,
		tim: TimHeader,
		palettes: ColorData[][],
		imageData?: ImageData,
	): void {
		const name = `${filename}_0x${ofs.toString(16).padStart(6, '0')}`;
		const sceneName = filename.startsWith('ST')
			? filename.substring(0, 4)
			: 'COMMON';
		this.scenes.put({ name: sceneName });

		// Calculate image page
		const { paletteX, paletteY, imageX, imageY, width, height } = tim;
		const imagePage = this.calcImagePage(imageX, imageY);

		let topLeft = false;
		let topRight = false;
		let bottomLeft = false;
		let bottomRight = false;

		if (width === 256 && height === 256) {
			topLeft = true;
			topRight = true;
			bottomLeft = true;
			bottomRight = true;
		} else if (width === 256 && height === 128) {
			const yOfs = imageY % 256;
			if (yOfs === 0) {
				topLeft = true;
				topRight = true;
			} else {
				bottomLeft = true;
				bottomRight = true;
			}
		} else {
			const xOfs = ((imageX - 320) % 64) * 4;
			const yOfs = imageY % 256;

			if (xOfs === 0 && yOfs === 0) {
				topLeft = true;
			} else if (xOfs > 0 && yOfs === 0) {
				topRight = true;
			} else if (xOfs === 0 && yOfs > 0) {
				bottomLeft = true;
			} else {
				bottomRight = true;
			}
		}

		this.textures.put({
			name,
			sceneName,
			width,
			height,
			imageX,
			imageY,
			imageData,
			imagePage,
			paletteX,
			paletteY,
			palettes,
			topLeft,
			topRight,
			bottomLeft,
			bottomRight,
		});
	}

	async selectEntities(sceneName: string): Promise<IEntity[]> {
		return await this.entities.where({ sceneName }).toArray();
	}

	async select(
		assetType: string,
		sceneName: string,
	): Promise<IEntity[] | ITexture[]> {
		switch (assetType) {
			case 'Entity':
				return await this.entities.where({ sceneName }).toArray();
			case 'Texture':
				return await this.textures.where({ sceneName }).toArray();
		}

		throw new Error('Invalid asset type provided');
	}

	pause(length = 300) {
		return new Promise((resolve) => {
			setTimeout(resolve, length);
		});
	}

	// Proprocess all of the files in the database to scan for assets
	async scan(
		setProgress: (p: number) => void,
		setFilename: (f: string) => void,
	): Promise<void> {
		// Get a list of all of the files stored in the database
		const files = await this.files.toArray();

		// For Each one of the files we check against each of the
		// asset types we want to scan for

		setProgress(0);
		for (let i = 0; i < files.length; i++) {
			setFilename(`Reading ${files[i].name}`);
			checkForEntities(files[i]);
			checkForTextures(files[i]);
			checkForPalettes(files[i]);
			setProgress((files.length / i) * 100);
			await this.pause();
		}
	}

	async scanFile(file: File): Promise<void> {
		const iFile = await this.readFile(file);
		checkForEntities(iFile);
		checkForTextures(iFile);
		checkForPalettes(iFile);
	}

	async selectImage(
		sceneName: string,
		image: FramebufferCoord,
	): Promise<ITexture | null> {
		if (image.name && image.name.length) {
			const query = { sceneName, name: image.name };
			const textures = await this.textures.where(query).toArray();
			return textures ? textures[0] : null;
		}

		const imagePage = this.calcImagePage(image.x, image.y);

		const query: {
			sceneName: string;
			imagePage: number;
			topLeft?: boolean;
			topRight?: boolean;
			bottomLeft?: boolean;
			bottomRight?: boolean;
		} = {
			sceneName,
			imagePage,
		};

		/*
		if(image.topLeft) {
			query.topLeft = true;
		}
		
		if(image.topRight) {
			query.topRight = true;
		}

		if(image.bottomLeft) {
			query.bottomLeft = true;
		}

		if(image.bottomRight) {
			query.bottomRight = true;
		}
		*/

		const textures = (await this.textures.where(query).toArray()) || [];
		const results = textures.filter((iTex) => {
			if (image.topLeft && !iTex.topLeft) {
				return false;
			}
			if (image.topRight && !iTex.topRight) {
				return false;
			}
			if (image.bottomLeft && !iTex.bottomLeft) {
				return false;
			}
			if (image.bottomRight && !iTex.bottomRight) {
				return false;
			}
			return true;
		});

		return results.length ? results[0] : null;
	}

	async selectImageByName(name: string): Promise<ITexture | null> {
		const query = { name };
		const textures = await this.textures.where(query).toArray();
		return textures ? textures[0] : null;
	}

	async selectPalette(
		sceneName: string,
		name: string | undefined,
		paletteX: number,
		paletteY: number,
	): Promise<ITexture | null> {
		const query = name
			? { sceneName, name }
			: { sceneName, paletteX, paletteY };
		const textures = (await this.textures.where(query).toArray()) || [];
		const results = textures.filter((iTex) => {
			return iTex.palettes.length > 0;
		});
		return results.length ? results[0] : null;
	}

	async selectImages(imageX: number, imageY: number): Promise<ITexture[]> {
		// Calculate image page
		const imagePage = this.calcImagePage(imageX, imageY);
		const query = { imagePage };
		const textures = await this.textures.where(query).toArray();
		const keys: string[] = [];
		return textures.filter((img) => {
			const key = JSON.stringify(img.imageData);
			if (keys.indexOf(key) !== -1) {
				return false;
			}
			keys.push(key);
			return true;
		});
	}

	async selectPalettes(
		paletteX: number,
		paletteY: number,
	): Promise<ITexture[]> {
		const query = { paletteX, paletteY };
		const textures = await this.textures.where(query).toArray();
		const keys: string[] = [];
		return textures.filter((img) => {
			const key = JSON.stringify(img.palettes);
			if (keys.indexOf(key) !== -1) {
				return false;
			}
			keys.push(key);
			return true;
		});
	}

	async selectTextures(coords: ImageCoordinate) {
		const { image, palette } = coords;
		const i = await this.selectImages(image.x, image.y);
		const p = await this.selectPalettes(palette.x, palette.y);
		return [i, p];
	}
}

const db = new FileStorage();
export default db;
