/**
 * Database Types
 **/

export interface IFile {
	name: string;
	data: ArrayBuffer;
}

export interface IScene {
	name: string;
}

export interface IEntity {
	sceneName: string;
	id: number;
	files: string[];
	data: ModelData;
}

export interface ITexture {
	name: string;
	sceneName: string;
	width: number;
	height: number;
	imageX: number;
	imageY: number;
	imageData: ImageData | undefined;
	imagePage: number;
	paletteX: number;
	paletteY: number;
	palettes: ColorData[][];
	topLeft: boolean;
	topRight: boolean;
	bottomLeft: boolean;
	bottomRight: boolean;
}

/**
 * Magic Numbers
 **/

export const MAGIC_PALETTE = 0x02;
export const MAGIC_IMAGE = 0x03;
export const MAGIC_ENTITY = 0x0a;

/**
 * Entity Types
 **/

export type EntityData = {
	filename: string;
	id: number;
	meshOfs: number;
	tracksOfs: number;
	controlOfs: number;
};

export type Vector2 = {
	x: number;
	y: number;
};

export type Vector3 = {
	x: number;
	y: number;
	z: number;
};

export type BoneData = {
	name: string;
	parent: number;
	pos: Vector3;
};

export type SegementData = {
	polygonIndex: number;
	boneParent: number;
	boneIndex: number;
	hidePolygon: boolean;
	shareVertices: boolean;
};

export type FramebufferCoord = {
	coords: number;
	x: number;
	y: number;
	name: string;
	index: number;
	topLeft: boolean;
	topRight: boolean;
	bottomLeft: boolean;
	bottomRight: boolean;
};

export type ImageCoordinate = {
	image: FramebufferCoord;
	palette: FramebufferCoord;
};

export type PolygonHeader = {
	faceTriCount: number;
	faceQuadCount: number;
	vertexCount: number;
	scale: number;
	triFaceOfs: number;
	quadFaceOfs: number;
	vertexOfs: number;
};

export type FaceIndex = {
	indice: number;
	u: number;
	v: number;
};

export type FaceData = {
	materialIndex: number;
	a: FaceIndex;
	b: FaceIndex;
	c: FaceIndex;
};

export type PolygonData = {
	faces: Array<FaceData>;
	vertices: Array<Vector3>;
};

export type LevelOfDetail = {
	high: PolygonData[];
	med: PolygonData[];
	low: PolygonData[];
};

export type ModelData = {
	lod: LevelOfDetail;
	textureCoords: Array<ImageCoordinate>;
	hierarchy: Array<SegementData>;
	skeleton: Array<BoneData>;
};

export type TimHeader = {
	decompressedSize: number;
	paletteX: number;
	paletteY: number;
	colorCount: number;
	paletteCount: number;
	imageX: number;
	imageY: number;
	width: number;
	height: number;
	bitfieldSize: number;
};

export type ColorData = {
	r: number;
	g: number;
	b: number;
	a: number;
};

export type PaletteData = ColorData[];
export type ImageData = number[];
