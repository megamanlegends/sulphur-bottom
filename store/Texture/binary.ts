import db from '../database';
import {
	MAGIC_IMAGE,
	IFile,
	TimHeader,
	ColorData,
	ImageData,
	MAGIC_PALETTE,
} from '../types';

const readTimdata = (tim: TimHeader, decompressedData: ArrayBuffer) => {
	const palettes = new Array<ColorData[]>();
	const imageData: ImageData = new Array();

	const view = new DataView(decompressedData);
	let ofs = 0;
	for (let i = 0; i < tim.paletteCount; i++) {
		const palette = new Array<ColorData>();
		palettes.push(palette);

		for (let k = 0; k < tim.colorCount; k++) {
			const word = view.getUint16(ofs, true);
			ofs += 0x02;
			const r = ((word >> 0x00) & 0x1f) << 3;
			const g = ((word >> 0x05) & 0x1f) << 3;
			const b = ((word >> 0x0a) & 0x1f) << 3;
			const a = word > 0 ? 1 : 0;
			const color: ColorData = { r, g, b, a };
			palette.push(color);
		}
	}

	while (ofs < decompressedData.byteLength) {
		const byte = view.getUint8(ofs++);
		if (tim.colorCount === 256) {
			imageData.push(byte);
		} else {
			imageData.push(byte & 0xf);
			imageData.push(byte >> 4);
		}
	}

	return { palettes, imageData };
};

const readBitField = (
	ofs: number,
	tim: TimHeader,
	view: DataView,
): ArrayBuffer => {
	// Read the bitfield into an array of 1's and 0's
	const bitField = [];
	for (let i = 0; i < tim.bitfieldSize; i += 4) {
		const dword = view.getUint32(ofs, true);
		ofs += 4;
		for (let k = 31; k > -1; k--) {
			bitField.push(dword & (1 << k) ? 1 : 0);
		}
	}

	// Prepare a buffer the size of the expected output
	const buffer = new ArrayBuffer(tim.decompressedSize);
	const fp = new DataView(buffer);

	// Then we decompress the bytes
	let outOfs = 0;
	let windowOfs = 0;

	for (let i = 0; i < bitField.length; i++) {
		let bit = bitField[i];
		let word = view.getUint16(ofs, true);
		if (bit === 0) {
			fp.setUint16(outOfs, word, true);
			outOfs += 2;
		} else if (word === 0xffff) {
			windowOfs += 0x2000;
		} else {
			let val = (word >> 3) & 0x1fff;
			let copyFrom = windowOfs + val;
			let copyLen = (word & 0x07) + 2;
			while (copyLen--) {
				let w = fp.getUint16(copyFrom, true);
				copyFrom += 2;
				fp.setUint16(outOfs, w, true);
				outOfs += 2;
			}
		}
		if (outOfs >= tim.decompressedSize) {
			break;
		}
		ofs += 2;
	}

	return buffer;
};

export const checkForTextures = (file: IFile): boolean => {
	const view = new DataView(file.data);
	let found = false;
	for (let ofs = 0; ofs < file.data.byteLength; ofs += 0x400) {
		const magic = view.getUint32(ofs, true);
		if (magic !== MAGIC_IMAGE) {
			continue;
		}
		const tim: TimHeader = {
			decompressedSize: view.getUint16(ofs + 0x04, true),
			paletteX: view.getUint16(ofs + 0x0c, true),
			paletteY: view.getUint16(ofs + 0x0e, true),
			colorCount: view.getUint16(ofs + 0x10, true),
			paletteCount: view.getUint16(ofs + 0x12, true),
			imageX: view.getUint16(ofs + 0x14, true),
			imageY: view.getUint16(ofs + 0x16, true),
			width: view.getUint16(ofs + 0x18, true),
			height: view.getUint16(ofs + 0x1a, true),
			bitfieldSize: view.getUint16(ofs + 0x24, true),
		};

		// We need to filter out false-positives
		if (
			tim.width > 256 ||
			tim.height > 256 ||
			tim.colorCount > 256 ||
			tim.width < 32 ||
			tim.height < 32
		) {
			continue;
		}

		switch (tim.colorCount) {
			case 16:
				tim.width *= 4;
				break;
			case 256:
				tim.width *= 2;
				break;
			default:
				tim.paletteCount *= tim.colorCount / 16;
				tim.colorCount = 16;
				tim.width *= 4;
				break;
		}

		const decompressedBytes = readBitField(ofs + 0x30, tim, view);
		const { palettes, imageData } = readTimdata(tim, decompressedBytes);

		db.insertTexture(file.name, ofs, tim, palettes, imageData);
		found = true;
	}

	return found;
};

export const checkForPalettes = (file: IFile): boolean => {
	const view = new DataView(file.data);
	let found = false;
	for (let ofs = 0; ofs < file.data.byteLength; ofs += 0x400) {
		const magic = view.getUint32(ofs, true);
		if (magic !== MAGIC_PALETTE) {
			continue;
		}
		const tim: TimHeader = {
			decompressedSize: view.getUint16(ofs + 0x04, true),
			paletteX: view.getUint16(ofs + 0x0c, true),
			paletteY: view.getUint16(ofs + 0x0e, true),
			colorCount: view.getUint16(ofs + 0x10, true),
			paletteCount: view.getUint16(ofs + 0x12, true),
			imageX: view.getUint16(ofs + 0x14, true),
			imageY: view.getUint16(ofs + 0x16, true),
			width: view.getUint16(ofs + 0x18, true),
			height: view.getUint16(ofs + 0x1a, true),
			bitfieldSize: view.getUint16(ofs + 0x24, true),
		};

		// We need to filter out false-positives
		if (
			tim.width !== 0 ||
			tim.height !== 0 ||
			tim.imageX !== 0 ||
			tim.imageY !== 0
		) {
			continue;
		}

		switch (tim.colorCount) {
			case 16:
				tim.width *= 4;
				break;
			case 256:
				tim.width *= 2;
				break;
			default:
				tim.paletteCount *= tim.colorCount / 16;
				tim.colorCount = 16;
				tim.width *= 4;
				break;
		}

		const palettes = new Array<ColorData[]>();
		let pOfs = ofs + 0x30;

		for (let i = 0; i < tim.paletteCount; i++) {
			const palette = new Array<ColorData>();
			palettes.push(palette);

			for (let k = 0; k < tim.colorCount; k++) {
				const word = view.getUint16(pOfs, true);
				pOfs += 0x02;
				const r = ((word >> 0x00) & 0x1f) << 3;
				const g = ((word >> 0x05) & 0x1f) << 3;
				const b = ((word >> 0x0a) & 0x1f) << 3;
				const a = word > 0 ? 1 : 0;
				const color: ColorData = { r, g, b, a };
				palette.push(color);
			}
		}

		db.insertTexture(file.name, ofs, tim, palettes);
		found = true;
	}

	return found;
};
