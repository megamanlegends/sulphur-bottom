import db from '../database';
import {
	MAGIC_ENTITY,
	EntityData,
	FaceData,
	FaceIndex,
	IFile,
	ModelData,
	PolygonData,
	PolygonHeader,
	Vector3,
	ImageCoordinate,
} from '../types';

const readModelDetails = (
	polygonCount: number,
	lodOfs: number,
	view: DataView,
	textureCoords: ImageCoordinate[],
): Array<PolygonData> => {
	const FACE_MASK = 0b1111111;
	const VERTEX_MASK = 0b1111111111;
	const VERTEX_MSB = 0b1000000000;
	const VERTEX_LOW = 0b0111111111;

	const PIXEL_TO_FLOAT_RATIO = 0.00390625;
	const PIXEL_ADJUSTMEST = 0.001953125;

	let ofs = lodOfs;
	const polygonHeaders: Array<PolygonHeader> = [];

	for (let i = 0; i < polygonCount; i++) {
		const scale = view.getInt8(ofs + 0x03);
		polygonHeaders.push({
			faceTriCount: view.getUint8(ofs + 0x00),
			faceQuadCount: view.getUint8(ofs + 0x01),
			vertexCount: view.getUint8(ofs + 0x02),
			scale: scale === -1 ? 0.5 : 1 << scale,
			triFaceOfs: view.getUint32(ofs + 0x04, true),
			quadFaceOfs: view.getUint32(ofs + 0x08, true),
			vertexOfs: view.getUint32(ofs + 0x0c, true),
		});
		ofs += 0x10;
	}

	const polygonData = polygonHeaders.map((polygonHeader) => {
		// Read Triangles

		const faces: Array<FaceData> = [];
		const vertices: Array<Vector3> = [];

		// Read the Triangles

		ofs = polygonHeader.triFaceOfs;
		for (let i = 0; i < polygonHeader.faceTriCount; i++) {
			const dword = view.getUint32(ofs + 0x08, true);
			const materialIndex = (dword >> 28) & 0x3;

			const au = view.getUint8(ofs + 0x00);
			const av = view.getUint8(ofs + 0x01);
			const bu = view.getUint8(ofs + 0x02);
			const bv = view.getUint8(ofs + 0x03);
			const cu = view.getUint8(ofs + 0x04);
			const cv = view.getUint8(ofs + 0x05);

			if(!textureCoords[materialIndex]) {
				textureCoords[materialIndex] = {
					image: {
						coords: 0,
						x: 0,
						y: 0,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
					palette: {
						coords: 0,
						x: 0,
						y: 0,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
				}
			}

			// Left
			if (au < 128 || bu < 128 || cu < 128) {
				// Top
				if (av < 128 || bv < 128 || cv < 128) {
					textureCoords[materialIndex].image.topLeft = true;
				}
				// Bottom
				if (av >= 128 || bv >= 128 || cv >= 128) {
					textureCoords[materialIndex].image.bottomLeft = true;
				}
			}

			// Right
			if (au >= 128 || bu >= 128 || cu >= 128) {
				// Top
				if (av < 128 || bv < 128 || cv < 128) {
					textureCoords[materialIndex].image.topRight = true;
				}
				// Bottom
				if (av >= 128 || bv >= 128 || cv >= 128) {
					textureCoords[materialIndex].image.bottomRight = true;
				}
			}

			const a: FaceIndex = {
				indice: (dword >> 0x00) & FACE_MASK,
				u: au * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: av * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			const b: FaceIndex = {
				indice: (dword >> 0x07) & FACE_MASK,
				u: bu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: bv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			const c: FaceIndex = {
				indice: (dword >> 0x0e) & FACE_MASK,
				u: cu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: cv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			faces.push({
				materialIndex,
				a: a,
				b: c,
				c: b,
			});
			ofs += 0x0c;
		}

		// Read the Quads

		ofs = polygonHeader.quadFaceOfs;
		for (let i = 0; i < polygonHeader.faceQuadCount; i++) {
			const dword = view.getUint32(ofs + 0x08, true);
			const materialIndex = (dword >> 28) & 0x3;

			const au = view.getUint8(ofs + 0x00);
			const av = view.getUint8(ofs + 0x01);
			const bu = view.getUint8(ofs + 0x02);
			const bv = view.getUint8(ofs + 0x03);
			const cu = view.getUint8(ofs + 0x04);
			const cv = view.getUint8(ofs + 0x05);
			const du = view.getUint8(ofs + 0x06);
			const dv = view.getUint8(ofs + 0x07);

			if(!textureCoords[materialIndex]) {
				textureCoords[materialIndex] = {
					image: {
						coords: 0,
						x: 0,
						y: 0,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
					palette: {
						coords: 0,
						x: 0,
						y: 0,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
				}
			}

			// Left
			if (au < 128 || bu < 128 || cu < 128 || du < 128) {
				// Top
				if (av < 128 || bv < 128 || cv < 128 || dv < 128) {
					textureCoords[materialIndex].image.topLeft = true;
				}
				// Bottom
				if (av >= 128 || bv >= 128 || cv >= 128 || dv >= 128) {
					textureCoords[materialIndex].image.bottomLeft = true;
				}
			}

			// Right
			if (au >= 128 || bu >= 128 || cu >= 128  || du >= 128) {
				// Top
				if (av < 128 || bv < 128 || cv < 128 || dv < 128) {
					textureCoords[materialIndex].image.topRight = true;
				}
				// Bottom
				if (av >= 128 || bv >= 128 || cv >= 128 || dv >= 128) {
					textureCoords[materialIndex].image.bottomRight = true;
				}
			}

			const a: FaceIndex = {
				indice: (dword >> 0x00) & FACE_MASK,
				u: au * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: av * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			const b: FaceIndex = {
				indice: (dword >> 0x07) & FACE_MASK,
				u: bu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: bv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			const c: FaceIndex = {
				indice: (dword >> 0x0e) & FACE_MASK,
				u: cu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: cv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			const d: FaceIndex = {
				indice: (dword >> 0x15) & FACE_MASK,
				u: du * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
				v: dv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
			};

			faces.push({
				materialIndex,
				a: a,
				b: c,
				c: b,
			});
			faces.push({
				materialIndex,
				a: b,
				b: c,
				c: d,
			});
			ofs += 0x0c;
		}

		// Read the Vertices

		ofs = polygonHeader.vertexOfs;
		for (let i = 0; i < polygonHeader.vertexCount; i++) {
			const dword = view.getUint32(ofs, true);
			ofs += 4;

			const xBytes = (dword >> 0x00) & VERTEX_MASK;
			const yBytes = (dword >> 0x0a) & VERTEX_MASK;
			const zBytes = (dword >> 0x14) & VERTEX_MASK;

			const xHigh = (xBytes & VERTEX_MSB) * -1;
			const xLow = xBytes & VERTEX_LOW;

			const yHigh = (yBytes & VERTEX_MSB) * -1;
			const yLow = yBytes & VERTEX_LOW;

			const zHigh = (zBytes & VERTEX_MSB) * -1;
			const zLow = zBytes & VERTEX_LOW;

			vertices.push({
				x: (xHigh + xLow) * polygonHeader.scale,
				y: (yHigh + yLow) * polygonHeader.scale,
				z: (zHigh + zLow) * polygonHeader.scale,
			});
		}

		return {
			faces,
			vertices,
		};
	});

	return polygonData;
};

const processEntity = (entity: EntityData, view: DataView): void => {
	const { filename, id } = entity;
	const data: ModelData = {
		lod: {
			high: [],
			med: [],
			low: [],
		},
		textureCoords: [],
		skeleton: [],
		hierarchy: [],
	};

	// Here we have two options, we can either update the pointers
	// of the original binary, or we can parse the entity into
	// a JSON object representation

	// I think that updating the pointers, and then trying to parse
	// binary later is increasing the surface area of things that
	// can go wrong. In this case we probably want to parse to JSON,
	// and that should make it easier to track down problems

	if (entity.meshOfs) {
		const { meshOfs } = entity;

		// Get the primitive count for each level-of-detail
		const highCount = view.getUint8(meshOfs + 0x00);

		// Read the Level-of-Detail Mesh Offsets
		const highOfs = view.getUint32(meshOfs + 0x04, true);

		// Get the offsets to the bones
		const bonesOfs = view.getUint32(meshOfs + 0x10, true);
		const heirarchyOfs = view.getUint32(meshOfs + 0x14, true);

		// Get offsets to the textures
		const textureOfs = view.getUint32(meshOfs + 0x18, true);
		const collisionOfs = view.getUint32(meshOfs + 0x1c, true);
		const shadowOfs = view.getUint32(meshOfs + 0x20, true);

		// Read Bones
		if (bonesOfs) {
			let ofs = bonesOfs;
			const nbBones = Math.floor((heirarchyOfs - bonesOfs) / 6);
			for (let i = 0; i < nbBones; i++) {
				data.skeleton.push({
					name: `bone_${i.toString().padStart(3, '0')}`,
					parent: -1,
					pos: {
						x: view.getInt16(ofs + 0x00, true),
						y: view.getInt16(ofs + 0x02, true),
						z: view.getInt16(ofs + 0x04, true),
					},
				});
				ofs += 6;
			}
		}

		// Read Textures
		if (textureOfs) {
			let ofs = textureOfs;
			const textureCount = ((collisionOfs || shadowOfs) - textureOfs) / 4;
			for (let i = 0; i < textureCount; i++) {
				const imageCoords = view.getUint16(ofs + 0x00, true);
				const paletteCoords = view.getUint16(ofs + 0x02, true);
				data.textureCoords.push({
					image: {
						coords: imageCoords,
						x: (imageCoords & 0x0f) << 6,
						y: imageCoords & 0x10 ? 0x100 : 0,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
					palette: {
						coords: paletteCoords,
						x: (paletteCoords & 0x3f) << 4,
						y: paletteCoords >> 6,
						name: '',
						index: -1,
						topLeft: false,
						topRight: false,
						bottomLeft: false,
						bottomRight: false,
					},
				});
				ofs += 4;
			}
		}

		// Read Hierarchy
		if (heirarchyOfs) {
			let ofs = heirarchyOfs;
			const nbSegments = (textureOfs - heirarchyOfs) / 4;
			for (let i = 0; i < nbSegments; i++) {
				const polygonIndex = view.getInt8(ofs + 0x00);
				const boneParent = view.getInt8(ofs + 0x01);
				const boneIndex = view.getUint8(ofs + 0x02);
				const flags = view.getUint8(ofs + 0x03);
				const hidePolygon = Boolean(flags & 0x80);
				const shareVertices = Boolean(flags & 0x40);

				if (flags & 0x3f) {
					console.log(
						`Unknown Flag: 0x${(flags & 0x3f).toString(16)}`,
					);
				}

				data.hierarchy.push({
					polygonIndex,
					boneParent,
					boneIndex,
					hidePolygon,
					shareVertices,
				});
				ofs += 4;
			}
		}

		// Parse the Raw Model Data
		data.lod.high = readModelDetails(
			highCount,
			highOfs,
			view,
			data.textureCoords,
		);
	}

	db.insertEntity(filename, id, data);
};

export const checkForEntities = (file: IFile): boolean => {
	// Entity sub-files are only declared at the top of the file

	const view = new DataView(file.data);
	const type = view.getUint32(0, true);
	const subFileLength = view.getUint32(0x04, true);

	// If it's not here, then we dont need to scan the rest of the file

	if (type !== MAGIC_ENTITY) {
		return false;
	}

	const entityCount = view.getUint32(0x30, true);

	// If the entity count has one of these bits, they are a single
	// entity and not a list of entities

	if (entityCount & 0x80000000 || entityCount & 0x40000000) {
		return false;
	}

	// We read the offsets of all of the entities declared in the
	// sub-file, so that we can seek and copy each one

	const entityList: Array<EntityData> = [];
	let ofs = 0x34;
	for (let i = 0; i < entityCount; i++) {
		entityList.push({
			filename: file.name,
			id: view.getUint32(ofs + 0x00, true),
			meshOfs: view.getUint32(ofs + 0x04, true),
			tracksOfs: view.getUint32(ofs + 0x08, true),
			controlOfs: view.getUint32(ofs + 0x0c, true),
		});
		ofs += 0x10;
	}

	// Next we want to seek to each one of the offsets and
	// slice out the entity data to store in the entity table

	const subFile = file.data.slice(0x30, subFileLength + 0x30);
	const subView = new DataView(subFile);
	entityList.forEach((entity) => {
		processEntity(entity, subView);
	});

	return true;
};
