import {
	Bone,
	BufferGeometry,
	Float32BufferAttribute,
	Matrix4,
	Mesh,
	MeshBasicMaterial,
	MeshPhongMaterial,
	Skeleton,
	Texture,
	Vector3,
	DoubleSide,
} from 'three';

import db from '../database';

import {
	BoneData,
	ImageCoordinate,
	LevelOfDetail,
	ModelData,
	PolygonData,
	SegementData,
} from '../types';

type DrawCall = {
	start: number;
	count: number;
	materialIndex: number;
};

const SCALE = 0.00125;
const ROT = new Matrix4();
ROT.makeRotationX(Math.PI);

const pause = () => {
	return new Promise((resolve) => {
		setTimeout(resolve, 100);
	});
};

const debugMats = [
	new MeshBasicMaterial({
		color: 0xff0000,
	}),
	new MeshBasicMaterial({
		color: 0xff00,
	}),
	new MeshBasicMaterial({
		color: 0xff,
	}),
	new MeshBasicMaterial({
		color: 0xffff00,
	}),
];

const resolveTextures = async (
	textureCoords: Array<ImageCoordinate>,
	sceneName: string,
): Promise<Array<MeshBasicMaterial>> => {
	const materials: Array<MeshBasicMaterial> = [];

	const array = [];
	for (let i = 0; i < textureCoords.length; i++) {
		const { image } = textureCoords[i];
		const imageData = await db.selectImage(sceneName, image);

		const { palette } = textureCoords[i];
		const paletteData = await db.selectPalette(
			sceneName,
			palette.name,
			palette.x,
			palette.y,
		);

		const [imgFile, imgOffset] = imageData!.name.split('_');
		const [palFile, palOffset] = paletteData!.name.split('_');
		array.push({
			imgFile,
			imgOffset,
			palFile,
			palOffset,
		});
		// If no image, provide debug image?
		if (!imageData) {
			console.warn('No image data found, skipping');
			materials.push(debugMats[i]);
			continue;
		}

		// If no palette, provide debug texture?
		if (!paletteData) {
			console.warn('No palette data found, skipping');
			materials.push(debugMats[i]);
			continue;
		}

		// Adding this to avoid possible compile time errors
		if (!document) {
			throw new Error('document global is not available');
		}

		// Create the canvas, context, and color set for the texture
		const canvas = document.createElement('canvas');
		canvas.width = 256;
		canvas.height = 256;
		const context = canvas.getContext('2d');
		const colors = paletteData!.palettes[0];

		const xOfs = ((imageData.imageX - 320) % 64) * 4;
		const yOfs = imageData.imageY % 256;

		let ofs = 0;
		for (let y = 0; y < imageData!.height; y++) {
			for (var x = 0; x < imageData!.width; x++) {
				const colorIndex = imageData!.imageData![ofs++];
				const { r, g, b, a } = colors[colorIndex];
				context!.fillStyle = `rgba(${r},${g},${b},${a})`;
				/*
				switch (i) {
					case 0:
						context!.fillStyle = 'purple';
						break;
					case 1:
						context!.fillStyle = 'pink';
						break;
					case 2:
						context!.fillStyle = 'orange';
						break;
					case 3:
						context!.fillStyle = 'black';
						break;
				}
				*/
				context!.fillRect(x + xOfs, y + yOfs, 1, 1);
			}
		}

		const texture = new Texture(canvas);
		texture.flipY = false;
		texture.needsUpdate = true;

		const mat = new MeshBasicMaterial({
			color: 0xffffff,
			map: texture,
			alphaTest: 0.05,
			transparent: true,
			side: DoubleSide,
		});
		// document.body.append(canvas);

		materials.push(mat);
	}

	console.log('Texture Array');
	const jsonStr = JSON.stringify(array, null, 2)
		.split('\n')
		.map((line) => {
			if (!line.includes('Offset')) {
				return line;
			}
			return line.replace(/"/g, '');
		})
		.join('\n');
	console.log(jsonStr);
	return materials;
};

export const entityMesh = async (
	data: ModelData,
	sceneName: string,
): Promise<Mesh> => {
	// First we figure out what kind of model this is
	// no skeleton - static mesh
	// with skeleton = rigged model

	await pause();
	const materials = await resolveTextures(data.textureCoords, sceneName);

	if (data.skeleton.length === 0) {
		console.log('static model');
	} else {
		console.log('rigged model');
	}

	const sourceData = data.lod.high;
	if (!data.skeleton.length) {
		return buildMesh(sourceData);
	}

	// Create list of threejs bones for skeleton
	const bones = data.skeleton.map((arm) => {
		const bone = new Bone();
		bone.name = arm.name;
		const { x, y, z } = arm.pos;
		const vec3 = new Vector3(x, y, z);
		vec3.multiplyScalar(SCALE);
		vec3.applyMatrix4(ROT);
		bone.position.x = vec3.x;
		bone.position.y = vec3.y;
		bone.position.z = vec3.z;
		return bone;
	});

	data.hierarchy.forEach((segment, index) => {
		const { boneIndex, boneParent } = segment;
		const bone = bones[boneIndex];
		if (index > 0 && bones[index]) {
			bones[boneParent].add(bone);
		}
	});

	// Update the local and world matrix for each bone
	bones.forEach((bone) => {
		bone.updateMatrix();
		bone.updateMatrixWorld();
	});

	const geometry = buildRiggedMesh(sourceData, bones, data.hierarchy);
	const skeleton = new Skeleton(bones);
	const mesh = new Mesh(geometry, materials);
	return mesh;
};

const buildMesh = (polygons: PolygonData[]) => {
	const geometry = new BufferGeometry();
	const indices: number[] = [];
	const vertices: number[] = [];
	const drawCalls: DrawCall[] = [];

	polygons.forEach((polygon) => {
		polygon.vertices.forEach((vertex) => {
			const { x, y, z } = vertex;
			const vec3 = new Vector3(x, y, z);
			vec3.multiplyScalar(SCALE);
			vec3.applyMatrix4(ROT);
			vertices.push(vec3.x, vec3.y, vec3.z);
		});

		polygon.faces.forEach((face) => {
			const { materialIndex, a, b, c } = face;
			indices.push(a.indice, c.indice, b.indice);
			const group = drawCalls[drawCalls.length - 1];
			if (!group) {
				drawCalls.push({
					start: 0,
					count: 3,
					materialIndex,
				});
				return;
			}

			if (group.materialIndex === materialIndex) {
				group.count += 3;
			} else {
				drawCalls.push({
					start: indices.length,
					count: 0,
					materialIndex,
				});
			}
		});
	});

	/*
    drawCalls.forEach( ({start, count, materialIndex}) => {
        geometry.addGroup(start, count, materialIndex);
    });
    */

	geometry.setIndex(indices);
	geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
	const materials = [
		new MeshPhongMaterial({
			color: 0x156289,
			flatShading: true,
			emissive: 0xe5b3c4,
		}),
		new MeshPhongMaterial({ color: 0xc4301d, flatShading: true }),
		new MeshPhongMaterial({ color: 0x0b9159, flatShading: true }),
		new MeshPhongMaterial({ color: 0xc6bc27, flatShading: true }),
	];

	return new Mesh(geometry, materials);
};

const buildRiggedMesh = (
	polygons: PolygonData[],
	bones: Bone[],
	hierarchy: SegementData[],
): BufferGeometry => {
	const geometry = new BufferGeometry();

	const vertices: number[] = [];
	const uvs: number[] = [];
	const drawCalls: DrawCall[] = [];

	polygons.forEach((polygon, index) => {
		const { boneIndex, boneParent, shareVertices, hidePolygon } =
			hierarchy[index];
		const bone = bones[boneIndex];
		const lookup: Vector3[] = [];

		polygon.vertices.forEach((vertex) => {
			const { x, y, z } = vertex;
			const vec3 = new Vector3(x, y, z);
			vec3.multiplyScalar(SCALE);
			vec3.applyMatrix4(ROT);
			vec3.applyMatrix4(bone.matrixWorld);
			lookup.push(vec3);
		});

		polygon.faces.forEach((face) => {
			const { materialIndex, a, b, c } = face;
			const aPoint = lookup[a.indice];
			const bPoint = lookup[b.indice];
			const cPoint = lookup[c.indice];

			const group = drawCalls[drawCalls.length - 1] || {
				start: 0,
				count: 0,
				materialIndex: -1,
			};

			if (group.materialIndex === materialIndex) {
				group.count += 3;
			} else {
				drawCalls.push({
					start: vertices.length / 3,
					count: 3,
					materialIndex,
				});
			}

			vertices.push(aPoint.x, aPoint.y, aPoint.z);
			vertices.push(bPoint.x, bPoint.y, bPoint.z);
			vertices.push(cPoint.x, cPoint.y, cPoint.z);
			uvs.push(a.u, a.v, b.u, b.v, c.u, c.v);
		});
	});

	drawCalls.forEach(({ start, count, materialIndex }) => {
		geometry.addGroup(start, count, materialIndex);
	});

	geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
	geometry.setAttribute('uv', new Float32BufferAttribute(uvs, 2));
	geometry.computeVertexNormals();
	return geometry;
};
