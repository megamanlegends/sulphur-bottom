export const names: {[key: string]: any} = {
    '1620' : {
        name : 'Verner Von Bluecher'
    },
    '1a20' : {
        name : 'Barrel'
    },
    '1c20' : {
        name : 'Megaman in Apron'
    },
    '1d20' : {
        name : 'Sulphur-Bottom Guard'
    },
    '1e20' : {
        name : 'Glyde'
    },
    '1f20' : {
        name : 'Roll in Pajamas'
    },
    '2120' : {
        name : 'Teisel Bonne'
    }
}