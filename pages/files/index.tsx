import React, { useEffect, CSSProperties, useState } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import db from '../../store/database';
import { Mesh } from 'three';

import MyNavbar from '../../components/MyNavbar';
import MyScene from '../../components/MyScene';
import MyTray from '../../components/MyTray';
import MyCanvas from '../../components/MyCanvas';
import MySidebar from '../../components/MySidebar';
import { IEntity } from '../../store/types';

const sidebarWidth = 400;

const mainStyle: CSSProperties = {
	display: 'flex',
	flexWrap: 'nowrap',
	height: '100vh',
	maxHeight: '100vh',
	overflowX: 'auto',
	overflowY: 'hidden',
	flexDirection: 'column',
};

const sectionStyle: CSSProperties = {
	display: 'flex',
	flexGrow: 1,
	flexDirection: 'row',
	position: 'relative',
};

const backdropStyle: CSSProperties = {
	backgroundColor: '#f8f8f8',
	width: `calc(100% - 460px - ${sidebarWidth}px)`,
	top: 0,
	left: '460px',
	height: '100%',
	position: 'absolute',
};

const asideStyle: CSSProperties = {
	backgroundColor: '#f8f8f8',
	width: `${sidebarWidth}px`,
	top: 0,
	right: '0px',
	height: '100%',
	position: 'absolute',
};

const Files: NextPage = () => {
	
	const [scene, setScene] = useState<string | undefined>();
	const [asset, setAsset] = useState<IEntity | undefined>()
	const [sceneList, setSceneList] = useState<string[]>([]);
	const [mesh, setMesh] = useState<Mesh | null>(null);

	useEffect(() => {
		if (!process.browser) {
			return;
		}

		db.getScenes().then((newSceneList: string[]) => {
			setSceneList(newSceneList);
			setScene(newSceneList[0]);
		});
	}, []);

	return (
		<div>
			<Head>
				<title>Sulphur Bottom | Files</title>
				<meta name='description' content='Read the Files from MML2' />
				<link rel='icon' href='/favicon.ico' />
			</Head>

			<main style={mainStyle}>
				<MyNavbar
					scene={scene}
					setScene={setScene}
					setSceneList={setSceneList}
				/>
				<section style={sectionStyle}>
					<MyScene
						scene={scene}
						setScene={setScene}
						sceneList={sceneList}
					/>
					<MyTray scene={scene} setMesh={setMesh} setAsset={setAsset}/>
					<div style={backdropStyle}>
						<MyCanvas mesh={mesh} />
					</div>
					<aside style={asideStyle}>
						<MySidebar asset={asset}/>
					</aside>
				</section>
			</main>
		</div>
	);
};

export default Files;
