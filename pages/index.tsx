import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import styles from '../styles/Home.module.css';

const Home: NextPage = () => {
	const myStyle = {
		page: {
			backgroundColor: '#e1e7ed',
		},
	};

	return (
		<div className={styles.container}>
			<Head>
				<title>Sulphur Bottom</title>
				<meta
					name='description'
					content='Splashscreen for selecting Megaman Legends 2 Tools'
				/>
				<link rel='icon' href='/favicon.ico' />
			</Head>

			<main className={styles.main} style={myStyle.page}>
				<h1 className={styles.title}>Sulphur Bottom</h1>

				<p className={styles.description}>
					Tools for analyzing assets from Megaman Legends 2
				</p>

				<div className={styles.grid}>
					<Link href='/files/'>
						<a className={styles.card}>
							<h2>Files &rarr;</h2>
							<p>
								Tools for reading assets directly from the Game
								Files
							</p>
						</a>
					</Link>

					<a href='https://nextjs.org/learn' className={styles.card}>
						<h2>Learn &rarr;</h2>
						<p>
							Learn about Next.js in an interactive course with
							quizzes!
						</p>
					</a>

					<a
						href='https://github.com/vercel/next.js/tree/master/examples'
						className={styles.card}
					>
						<h2>Examples &rarr;</h2>
						<p>
							Discover and deploy boilerplate example Next.js
							projects.
						</p>
					</a>

					<a
						href='https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
						className={styles.card}
					>
						<h2>Deploy &rarr;</h2>
						<p>
							Instantly deploy your Next.js site to a public URL
							with Vercel.
						</p>
					</a>
				</div>
			</main>
		</div>
	);
};

export default Home;
